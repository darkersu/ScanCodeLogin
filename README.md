## 1：仿微信，QQ，微博扫码登录

一直都感觉扫码登录很神奇啊！！然后谷歌一搜思路一看，发现还真是挺简单的，所以技痒自己写个玩玩，代码里面的实现方式和思路可以借鉴。不要问为什么不用`PHP`,`Java`，我不会告诉你`Node.js`写起来更简单更方便，至于用什么后端语言实现这个不是关键，看你个人爱好。

**哦，对了。这里实现的是扫码登录的一整套代码，看懂了拿过去改改封装一下就可以在项目中使用了。这其中包含服务端(NodeServer)，App端(UniAppMobile)，Web端(VueQRCode)，可以直接部署跑起来哦。
不过你要先准备好以下环境并且掌握不少的前后端技术点才能愉快玩耍哦：**

- Nodejs
- Mysql
- Redis
- Uni-app
- Socket
- Express
- Vue.js

来自大佬 [尤雨溪](https://baike.baidu.com/item/%E5%B0%A4%E9%9B%A8%E6%BA%AA/2281470?fr=aladdin) 的鼓励：

![haha](./Resources/haha.jpeg)

无图无真相，下面请接招：

**效果图：**

1.1 Web端未扫码：
![web-start](./Resources/web-start.png)

1.2 App端已扫码，但未在App端确认登录：
![web-start](./Resources/web-ing.png)

1.3 Web端二维码失效：
![web-start](./Resources/web-end.png)

1.4 Web端扫码成功：
![web-start](./Resources/web-success.png)

1.5 App端首页
![web-start](./Resources/app-index.png)

1.6 App端扫码页面
![app-scan](./Resources/app-scan.jpg)

1.7 App端确认登录
![web-start](./Resources/app-login.png)


## 2：实现的功能

额，类似你用过的`QQ`，`微信`，`微博`扫码登录的那些功能，简单说下：

- [x] web端从服务器获取二维码和uuid
- [x] web端处理二维码失效，重复扫码等
- [x] redis控制key的超时时间，前端二维码超时样式控制
- [x] App端扫码成功后，web端的样式控制和用户信息显示
- [x] App端首页扫码后进入login页面确认登录
- [x] 二维码失效后，可以点击重新获取
- [x] App端首页扫码后通知 --> 服务器--> web端 展示最新扫码进度
- [x] 已通过扫码登录后，可以使用【移动端授权登录】来唤醒App对web端登录进行授权

## 3 实现思路

![img](./Resources/silu.jpg)

## 4：项目结构

下面先看下项目的基本结构，后面再说实现思路。

### 4.1 NodeServer - 服务端

主要用`Node.js`实现后端，通过`socket.io`+`redis`实现`web`端的二维码生成，`uuid`等，同时还实现了`android`端的扫码登录的接口等。具体看下面目录结构

```text
├── BroadcastChannel  # 广播频道，主要为了让在socket.io中能及时知道 redis 订阅key的失效事件
│   └── index.js
├── app.js            # express 框架的启动入口文件
├── bin               # express 框架的启动脚本文件
│   └── www
├── mysql             # mysql的封装，主要使用了我自己开发的npm插件"yn-mysql-utils"
│   └── index.js
├── node_modules      # 依赖包，没什么好说的
├── package.json      # 项目依赖配置文件，没什么好说的
├── redis             # redis 的简单封装
│   └── index.js
├── routes            # 路由接口
│   ├── index.js        # 这里面实现的接口主要给APP端使用
│   └── socket.js       # 实现socket.io，主要给Vue的web端使用
└── utils             # 工具类的封装
    ├── index.js
```
### 4.2 UniAppMobile - Android App端

使用`uni-app`开发的`android`端，主要为了实现扫码然后把`App`上已登录的用户信息GET传到`redis`中。没什么，很简单。主要逻辑在：

- pages/index/index.vue
- pages/login/login.vue 

如果`uni-app`不会，请自行看官方文档学习：

> [uni-app](https://uniapp.dcloud.io/)

### 4.3 VueQRCode - Vue web端

主要使用`Vue`写的`Web`，代码里面注释很多，看看就明白了。主要的代码逻辑都在这个`web`端中。

## 5 关于我

一个热爱编程喜欢折腾的人，一个曾经的后端但如今迷失在前端技术中的人，一个喜欢各种技术的人，更多开源项目请看：

- [HtmlToPug](https://gitee.com/bmycode/HtmlToPug)

- [Whtml](https://www.npmjs.com/package/whtml)

- [Vue3Template](https://github.com/helpcode/Vue3Template)

- [vue3decorators](https://www.npmjs.com/package/vue3decorators)

- [vue-cli-plugin-autorouter](https://www.npmjs.com/package/vue-cli-plugin-autorouter)